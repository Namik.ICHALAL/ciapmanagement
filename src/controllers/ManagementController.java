package controllers;

import app.Main;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class ManagementController {
    @FXML
    private ImageView startImage;

    @FXML
    private JFXButton homeBT;

    @FXML
    private ImageView homeImageV;




    private Stage dialogStage;
private Main main;

    public void setDialogStage(Stage managmentHome) {
        dialogStage = managmentHome;
    }

    public void setMainApp(Main main) {
        this.main = main;
        startImage.setImage(new Image("/photos/applicationStart.jpg"));
        homeImageV.setImage(new Image("/icons/home.png"));


    }
}
