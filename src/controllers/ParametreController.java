package controllers;

import app.Main;
import javafx.fxml.FXML;
import javafx.stage.Stage;

public class ParametreController {
    private Stage dialogStage;
    private Main main;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMain(Main main) {
        this.main = main;}

    @FXML
    public void returnToHome() {
        main.showHome();
    }
    @FXML
    public void returnToPlanning() {
        main.showPlanning();
    }
}
