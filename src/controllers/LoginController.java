package controllers;


import app.Main;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;


public class LoginController {

    @FXML
    private JFXButton connexionBT;

    @FXML
    private JFXTextField userTF;

    @FXML
    private JFXPasswordField passWordTF;

    @FXML
    private ImageView connexionImage;


    private Main main;
    private Stage dialogStage;
private boolean isChecked ;


    public void setDialogStage(Stage loginStageUser) {
        dialogStage = loginStageUser;
    }
    public void setMain(Main main) {
        this.main=main;
        connexionImage.setImage(new Image("/icons/userConnexion.png"));    }

@FXML
   private void userVerification ()
{
    isChecked = true;
    dialogStage.close();
}
    public boolean isChecked() {
return isChecked;
    }




}