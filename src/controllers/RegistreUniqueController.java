package controllers;

import app.Main;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;


public class RegistreUniqueController {
    @FXML
    private ScrollPane scrollPane;
    private JFXTreeTableView  registreTV = new JFXTreeTableView();



    private JFXTreeTableColumn  nomPrenomColumn = new JFXTreeTableColumn("Nom et Prénom du Salarié");

    private JFXTreeTableColumn   nSecuriteSocaileColumn= new JFXTreeTableColumn("N° sécurité Sociale");

    private JFXTreeTableColumn   nationaliteColumn= new JFXTreeTableColumn("Nationalité");

    private JFXTreeTableColumn   dateDeNaissanceColumn= new JFXTreeTableColumn("Date de Naissance");

    private JFXTreeTableColumn   sexeColumn= new JFXTreeTableColumn("Sexe");

    private JFXTreeTableColumn   emploiColumn= new JFXTreeTableColumn("Emploi");

    private JFXTreeTableColumn   qualificationProfessionelleColumn= new JFXTreeTableColumn("Qualification Professionnelle");

    private JFXTreeTableColumn   DatesColumn= new JFXTreeTableColumn("Dates");

    private JFXTreeTableColumn   dEntreeColumn= new JFXTreeTableColumn(" D'entrée");

    private JFXTreeTableColumn   dSortieColumn= new JFXTreeTableColumn("De Sortie");

    private JFXTreeTableColumn   dateDesAutorisationsColumn= new JFXTreeTableColumn("Dates des Autorisations");

    private JFXTreeTableColumn   dEmbaucheColumn= new JFXTreeTableColumn("D'Embauche");

    private JFXTreeTableColumn   deLicenciementColumn= new JFXTreeTableColumn("De Licenciement");

    private JFXTreeTableColumn   travailleursEtrangersColumn= new JFXTreeTableColumn("Pour les travailleurs étrangers assujettis a la possession d'un titre autorisdant l'exercice d'une activité salariée");


    private JFXTreeTableColumn   typeDeDocumentColumn= new JFXTreeTableColumn("Type du document");

    private JFXTreeTableColumn   nDOrdreDocumentColumn= new JFXTreeTableColumn("Numéro d'ordre du document");

    private JFXTreeTableColumn   contratAprentissageColumn= new JFXTreeTableColumn("Pour les titulaires d'un Contrat d'Apprentissage ou de Professionnalisation");

    private JFXTreeTableColumn   apprentiColumn= new JFXTreeTableColumn("Apprenti");

    private JFXTreeTableColumn   contratProfesionnalisationColumn= new JFXTreeTableColumn("Contrat de Professionnalisation");

    private JFXTreeTableColumn   contratSpecifiqueColumn= new JFXTreeTableColumn("Pour les travailleurs a contrat spécifique");

    private JFXTreeTableColumn   cDDColumn= new JFXTreeTableColumn("Contrat a Durée Déterminée");

    private JFXTreeTableColumn   sTempsPartielColumn= new JFXTreeTableColumn("Salarié à Temps Partiel");

    private JFXTreeTableColumn   sTemporaireColumn= new JFXTreeTableColumn("Salarié Temporaire");

    private JFXTreeTableColumn   nomAdresseEColumn= new JFXTreeTableColumn("Nom et Adresse de l'entreprise de travail temporaire");

    private JFXTreeTableColumn   groupementEmployeursColumn= new JFXTreeTableColumn("Mis à disposition par un groupement d'employeurs");

    private JFXTreeTableColumn   nomAdresseGEColumn= new JFXTreeTableColumn("Nom et Adresse du groupement D'employeurs");
    private Stage dialogStage;
    private Main main;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMain(Main main) {
        this.main = main;


 registreTV.getColumns().addAll(nomPrenomColumn,nSecuriteSocaileColumn, nationaliteColumn, dateDeNaissanceColumn, sexeColumn, emploiColumn,
         qualificationProfessionelleColumn, DatesColumn,dateDesAutorisationsColumn,travailleursEtrangersColumn,
         contratAprentissageColumn, contratSpecifiqueColumn);

        DatesColumn.getColumns().addAll( dEntreeColumn,dSortieColumn);
        dateDesAutorisationsColumn.getColumns().addAll(dEmbaucheColumn,deLicenciementColumn);
        travailleursEtrangersColumn.getColumns().addAll(typeDeDocumentColumn, nDOrdreDocumentColumn);
        contratAprentissageColumn.getColumns().addAll( apprentiColumn, contratProfesionnalisationColumn);
        contratSpecifiqueColumn.getColumns().addAll( cDDColumn,sTempsPartielColumn, sTemporaireColumn, nomAdresseEColumn, groupementEmployeursColumn, nomAdresseGEColumn);
        scrollPane.setContent(registreTV);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

         nomPrenomColumn.setMinWidth(200);

       nSecuriteSocaileColumn.setMinWidth(150);
       nationaliteColumn.setMinWidth(100);
        dateDeNaissanceColumn.setMinWidth(150);
       sexeColumn.setMinWidth(80);
         emploiColumn.setMinWidth(150);
          qualificationProfessionelleColumn.setMinWidth(200);
       DatesColumn.setMinWidth(300);
          dEntreeColumn.setMinWidth(150);
          dSortieColumn.setMinWidth(150);
         dateDesAutorisationsColumn.setMinWidth(300);
       dEmbaucheColumn.setMinWidth(150);
        deLicenciementColumn.setMinWidth(150);
           travailleursEtrangersColumn.setMinWidth(300);
          typeDeDocumentColumn.setMinWidth(150);
         nDOrdreDocumentColumn.setMinWidth(150);
          contratAprentissageColumn.setMinWidth(300);
          apprentiColumn.setMinWidth(150);
        contratProfesionnalisationColumn.setMinWidth(150);
         contratSpecifiqueColumn.setMinWidth(900);
          cDDColumn .setMinWidth(150);
         sTempsPartielColumn.setMinWidth(150);
         sTemporaireColumn.setMinWidth(150);
       nomAdresseEColumn.setMinWidth(150);
        groupementEmployeursColumn.setMinWidth(150);
         nomAdresseGEColumn.setMinWidth(150);










    }


    public void returnToPlanning() {
        main.showPlanning();
    }

    public void returnToHome(ActionEvent actionEvent) {
        main.showHome();
    }
}
