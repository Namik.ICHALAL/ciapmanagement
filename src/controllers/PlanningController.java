package controllers;

import app.Main;
import javafx.stage.Stage;

public class PlanningController {
    private Stage dialogStage;
    private Main main;
    public void setDialogStage(Stage primaryStage) {
        this.dialogStage = primaryStage;
    }


    public void setMain(Main main) {
        this.main = main;
    }

    public void returnToHome() {
        main.showHome();
    }
}
