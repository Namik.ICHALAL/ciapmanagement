package controllers;

import app.Main;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.MenuButton;
import javafx.stage.Stage;
import metier.*;
import services.PersonnesBeans;

import java.util.ArrayList;

public class EditPersonneController {
    @FXML
    private JFXTextField nomTF;

    @FXML
    private JFXTextField prenomTF;

    @FXML
    private JFXTextField jourTF;

    @FXML
    private JFXTextField nSecuriteSociale;

    @FXML
    private JFXTextField nCarteProTF;

    @FXML
    private JFXTextField telephoneTF;

    @FXML
    private JFXTextField emailTF;

    @FXML
    private JFXTextField adresse1TF;

    @FXML
    private JFXTextField adresse2TF;

    @FXML
    private JFXTextField codePostalTF;

    @FXML
    private JFXComboBox<VilleH> villeCB;

    @FXML
    private JFXComboBox<PaysH> paysCB;

    @FXML
    private JFXTextField nationaliteTF;

    @FXML
    private JFXTextField moisTF;

    @FXML
    private JFXTextField anneeTF;

    @FXML
    private MenuButton qualificationsMB;

    @FXML
    private JFXTextField jourDebutContratTF;

    @FXML
    private JFXTextField moisDebutContratTF;

    @FXML
    private JFXTextField anneeDebutContratTF;

    @FXML
    private JFXTextField jourFinContratTF;

    @FXML
    private JFXTextField moisFinContratTF;

    @FXML
    private JFXTextField anneeFinContratTF;

    @FXML
    private JFXComboBox<TypeDeContratH> typeContratCB;
    private ObservableList<QualificationProfesionnelleH> qualifications;

    @FXML
    private JFXComboBox<EmploiH> fonctionCB;
  private PersonnesBeans personnesBeans = new PersonnesBeans();
    private Main main;
private Stage dialogStage;

    public void setMain(Main main) {
        this.main = main;
        PersonneH personne = new PersonneH();
        typeContratCB.setItems(personnesBeans.getTypeContrats());
        fonctionCB.setItems(personnesBeans.getEmploisList());
        qualifications= personnesBeans.getQualifications();
        System.out.println(qualifications);
        for  (QualificationProfesionnelleH qualifiaction : qualifications){
            CheckMenuItem  menuItem = new CheckMenuItem(qualifiaction.getNomQualificationProfesionnelle());
            System.out.println(qualifiaction);
            qualificationsMB.getItems().add(menuItem);
 villeCB.setDisable(true);


        }

    }
@FXML
    private void displayCityZipCode() {
        System.out.println("gestionnaire appelé");
        if(!codePostalTF.getText().isEmpty() && codePostalTF.getText().length() == 5){
            System.out.println("données recupérées");
            villeCB.setDisable(false);
            villeCB.setItems(personnesBeans.getCitiesZipCode(codePostalTF.getText()));
        }
    }

    public void setDialogStage(Stage editStage) {
        dialogStage = editStage;
    }


}
