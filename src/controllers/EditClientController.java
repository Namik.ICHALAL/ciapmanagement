package controllers;

import app.Main;
import javafx.stage.Stage;

public class EditClientController {
    private Main main;
    private Stage dialogStage;


    public void setMain(Main main) {
        this.main= main;
    }

    public void setDialogStage(Stage editStage) {
        dialogStage = editStage;
    }
}
