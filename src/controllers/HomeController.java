package controllers;

import app.Main;
import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.scene.control.MenuButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class HomeController {


    @FXML
    private JFXButton planningB;

    @FXML
    private ImageView planningImageV;

    @FXML
    private JFXButton personnelB;

    @FXML
    private ImageView personnelsImageV;

    @FXML
    private JFXButton facturationB;

    @FXML
    private ImageView facturationsImageV;

    @FXML
    private JFXButton parametresB;

    @FXML
    private ImageView parametresImageV;
    @FXML
    private ImageView logoImageV;

    @FXML

    private MenuButton MenuButton;


    @FXML
    private JFXButton homeBT;

    private Stage dialogStage;
    private Main main;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMain(Main main) {
        this.main = main;

        homeBT.setTooltip(new Tooltip("Accueil"));
    }
@FXML
    private void insertPlanningScene(){
        main.showPlanning();
}

@FXML
    public void insertRoussourcesHumainesScene() {
main.showRessourcesHumaines();
    }
    @FXML
    public void insertParametresScene() {
        main.showParametres();
    }
    @FXML
    public void returnToHome() {
        main.showHome();
    }
    @FXML
    public void returnToPlanning() {
        main.showPlanning();
    }

    public void insertRegistreUniqueScene() {
        main.showRegistreUnique();
    }
}
