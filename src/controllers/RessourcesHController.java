package controllers;

import app.Main;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import metier.ClientH;
import metier.ContratH;
import metier.EmploiH;
import metier.PersonneH;
import services.ClientBeans;
import services.ClientSearched;
import services.PersonneSearched;
import services.PersonnesBeans;


public class RessourcesHController {
    private Stage dialogStage;
    private Main main;
    private PersonnesBeans personnesBeans= new PersonnesBeans();
    @FXML
    private TableView personnesTV;

    @FXML
    private TableColumn<PersonneH,String> nomPerColumn;

    @FXML
    private TableColumn<PersonneH, String> prenomPerColumn;
    @FXML
    private TableColumn<PersonneH, String> fonctionColumn;
    @FXML
    private TableColumn<PersonneH, String>nationaliteColumn;
    @FXML
    private  TableView  clientsTV;
    @FXML
    private TableColumn<ClientH, String>  formeJuridiqueColumn;
    @FXML
    private TableColumn<ClientH, String> raisonSocialeColumn;

    @FXML
    private TableColumn<ClientH, String> numeroSiretColumn;
    @FXML
    private Label nomClientLable;

    @FXML
    private Label prenomClientLable;

    @FXML
    private Label nClientLable;

    @FXML
    private Label fJuridiqueLable;

    @FXML
    private Label raisonSocialeLabel;

    @FXML
    private Label adresseClientLabel;

    @FXML
    private Label villeClientLabel;

    @FXML
    private Label paysClientLabel;

    @FXML
    private Label nSiretLabel;

    @FXML
    private Label nTVALabel;

    @FXML
    private Label nAgentLabel;

    @FXML
    private Label nomLabel;

    @FXML
    private Label prenomLabel;

    @FXML
    private Label adresseLabel;

    @FXML
    private Label nSecuriteSocialeLabel;

    @FXML
    private Label fonctionLabel;

    @FXML
    private Label nCarteProLabel;

    @FXML
    private Label typeContratLabel;


    @FXML
    private JFXTextField nomTF;
    private PersonneSearched personneSearched ;
    private ClientSearched clientSearched;

private ClientBeans clientBeans = new ClientBeans();
    @FXML
    private JFXComboBox<EmploiH> fonctionCB;
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMain(Main main) {
        this.main = main;

        personnesTV.setItems(personnesBeans.getSalaries());
        nomPerColumn.setCellValueFactory((TableColumn.CellDataFeatures<PersonneH, String> feature) -> feature.getValue().nomPerProperty());
        prenomPerColumn.setCellValueFactory((TableColumn.CellDataFeatures<PersonneH, String> feature) -> feature.getValue().prenomPerProperty());
     fonctionColumn.setCellValueFactory(( (TableColumn.CellDataFeatures<PersonneH, String> feature) ->
              feature.getValue().getContrats().get(0).getEmploi().emploiProperty()));
    nationaliteColumn.setCellValueFactory((TableColumn.CellDataFeatures<PersonneH, String> feature) -> feature.getValue().getNationalite().nationaliteProperty());
    personnesTV.getSelectionModel().selectedItemProperty()
            .addListener((observable, oldValue, personneSelected) -> { afficherDetailPersonne((PersonneH) personneSelected);});
   fonctionCB.setItems(personnesBeans.getEmploisList());
          clientsTV.setItems(clientBeans.getClients());
          raisonSocialeColumn.setCellValueFactory((TableColumn.CellDataFeatures<ClientH, String> feature) -> feature.getValue().raisonSocialeProperty());
          numeroSiretColumn.setCellValueFactory((TableColumn.CellDataFeatures<ClientH, String> feature) -> feature.getValue().numeroSiretProperty());
          formeJuridiqueColumn.setCellValueFactory((TableColumn.CellDataFeatures<ClientH, String> feature) -> feature.getValue().getFormejuridique().formeJuridiqueProperty());
        clientsTV.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, clientSelected) -> { afficherDetailClients((ClientH) clientSelected);});
    }
    // display Client selected detail
    private void afficherDetailClients(ClientH clientSelected) {
       ClientH client = clientBeans.getClientById(clientSelected.getIdClient());

        nClientLable.setText(String.valueOf(client.getIdClient()));
        nomClientLable.setText(client.getNom());
        prenomClientLable.setText(client.getPrenom());
        raisonSocialeLabel.setText(client.getRaisonSociale());
        fJuridiqueLable.setText(client.getFormejuridique().getFormeJuridique());
        nSiretLabel.setText(client.getNumeroSiret());
        nTVALabel.setText(client.getNumeroTVA());
        villeClientLabel.setText(client.getVille().getNomVille());
       paysClientLabel.setText(client.getVille().getPays().getNomPays());

        if (client.getAdresse2() == null)
        {adresseClientLabel.setText(client.getAdresse1());}
 else {adresseClientLabel.setText(client.getAdresse1()+ client.getAdresse2());}

    }


    // return to home
    @FXML
    public void returnToHome() {
        main.showHome();
    }
    // go to planning scene
    @FXML
    public void returnToPlanning() {
        main.showPlanning();
    }
    // show add Personne
    @FXML
    public void  addPersonne(){
        main.showEditPersonne();
    }
    // show add Client
    @FXML
    public void addClient(){
        main.showEditClient();
    }
// display personne selected detail

    private void afficherDetailPersonne(PersonneH personneSelected) {
        System.out.println(personneSelected);
        personneSearched = new PersonneSearched();
        personneSearched.setIdPersonne(personneSelected.getIdPersonne());
        PersonneH personne = personnesBeans.getById(personneSearched);
        if (personne!=null){
            nAgentLabel.setText(String.valueOf(personne.getIdPersonne()));
            nomLabel.setText(personne.getNom());
            prenomLabel.setText(personne.getPrenom());
            adresseLabel.setText(personne.getAdresse1()+personne.getAdresse2());
            nSecuriteSocialeLabel.setText(personne.getNumeroSecuriteSociale());
            fonctionLabel.setText(personne.getContrats().get(0).getEmploi().getLibelleEmploi());
            nCarteProLabel.setText(personne.getNumeroCartePro());
            typeContratLabel.setText(personne.getContrats().get(0).getTypeContratH().getTypeDeContrat());

        }
    }
}


