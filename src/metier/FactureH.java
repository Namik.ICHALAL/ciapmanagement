package metier;


import java.util.Date;

public class FactureH {
    private int idFacture;
    private String libelleFacture;
    private Date dateFacturation;
    private Date datePaiement;
    private boolean reglement;


    public int getIdFacture() {
        return idFacture;
    }

    public void setIdFacture(int idFacture) {
        this.idFacture = idFacture;
    }

    public String getLibelleFacture() {
        return libelleFacture;
    }

    public void setLibelleFacture(String libelleFacture) {
        this.libelleFacture = libelleFacture;
    }

    public Date getDateFacturation() {
        return dateFacturation;
    }

    public void setDateFacturation(Date dateFacturation) {
        this.dateFacturation = dateFacturation;
    }

    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public boolean isReglement() {
        return reglement;
    }

    public void setReglement(boolean reglement) {
        this.reglement = reglement;
    }


}
