package metier;


import java.util.Date;

public class VaccinH {
    private int idVaccin;
    private String nomVaccin;
    private Date dateVaccin;
    private Date dureeProchainVaccin;


    public int getIdVaccin() {
        return idVaccin;
    }

    public void setIdVaccin(int idVaccin) {
        this.idVaccin = idVaccin;
    }


    public String getNomVaccin() {
        return nomVaccin;
    }

    public void setNomVaccin(String nomVaccin) {
        this.nomVaccin = nomVaccin;
    }


    public Date getDateVaccin() {
        return dateVaccin;
    }

    public void setDateVaccin(Date dateVaccin) {
        this.dateVaccin = dateVaccin;
    }


    public Date getDureeProchainVaccin() {
        return dureeProchainVaccin;
    }

    public void setDureeProchainVaccin(Date dureeProchainVaccin) {
        this.dureeProchainVaccin = dureeProchainVaccin;
    }


}
