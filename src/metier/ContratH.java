package metier;


import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.util.Date;

public class ContratH {
    private int numeroContrat;
    private Date dateDebut;
    private Date dateFin;
  private ObjectProperty<EmploiH> emploi;
  private TypeDeContratH typeContratH;
    public ContratH() {
emploi= new SimpleObjectProperty<>();
    }

    public int getNumeroContrat() {
        return numeroContrat;
    }

    public void setNumeroContrat(int numeroContrat) {
        this.numeroContrat = numeroContrat;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public EmploiH getEmploi() {
        return emploi.get();
    }

    public ObjectProperty<EmploiH> emploiProperty() {
        return emploi;
    }

    public void setEmploi(EmploiH emploi) {
        this.emploi.set(emploi);
    }

    public TypeDeContratH getTypeContratH() {
        return typeContratH;
    }

    public void setTypeContratH(TypeDeContratH typeContratH) {
        this.typeContratH = typeContratH;
    }

    @Override
    public String toString() {
        return "ContratH{" +
                "numeroContrat=" + numeroContrat +
                ", dateDebut=" + dateDebut +
                ", dateFin=" + dateFin +
                ", emploi=" + emploi +
                ", typeContratH=" + typeContratH +
                '}';
    }
}
