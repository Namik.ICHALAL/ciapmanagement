package metier;


public class DetenirH {
    private int idQualificationProfesionnelle;
    private int idPersonne;


    public int getIdQualificationProfesionnelle() {
        return idQualificationProfesionnelle;
    }

    public void setIdQualificationProfesionnelle(int idQualificationProfesionnelle) {
        this.idQualificationProfesionnelle = idQualificationProfesionnelle;
    }

    public int getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(int idPersonne) {
        this.idPersonne = idPersonne;
    }


}
