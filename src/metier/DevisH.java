package metier;


import java.util.Date;

public class DevisH {
    private int idDevis;
    private String libelleDevis;
    private Date dateCreation;

    public int getIdDevis() {
        return idDevis;
    }

    public void setIdDevis(int idDevis) {
        this.idDevis = idDevis;
    }


    public String getLibelleDevis() {
        return libelleDevis;
    }

    public void setLibelleDevis(String libelleDevis) {
        this.libelleDevis = libelleDevis;
    }


    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }


}
