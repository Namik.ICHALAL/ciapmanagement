package metier;



public class TypeDeContratH {
    private int idContrat;
    private String typeDeContrat;

    public TypeDeContratH() {

    }

    public int getIdContrat() {
        return idContrat;
    }

    public void setIdContrat(int idContrat) {
        this.idContrat = idContrat;
    }


    public String getTypeDeContrat() {
        return typeDeContrat;
    }

    public void setTypeDeContrat(String typeDeContrat) {
        this.typeDeContrat = typeDeContrat;
    }

    @Override
    public String toString() {
        return  typeDeContrat ;
    }
}
