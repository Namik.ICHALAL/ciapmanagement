package metier;



public class EnregistrerH {
    private int numeroContrat;
    private int idUtilisateur;


    public int getNumeroContrat() {
        return numeroContrat;
    }

    public void setNumeroContrat(int numeroContrat) {
        this.numeroContrat = numeroContrat;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }


}
