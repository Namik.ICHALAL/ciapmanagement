package metier;



public class ClasserH {
    private int idMainCourante;
    private int idUtilisateur;


    public int getIdMainCourante() {
        return idMainCourante;
    }

    public void setIdMainCourante(int idMainCourante) {
        this.idMainCourante = idMainCourante;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }


}
