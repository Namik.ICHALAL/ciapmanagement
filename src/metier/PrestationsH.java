package metier;


import java.util.Date;

public class PrestationsH {
    private int idPrestation;
    private Date jour;
    private Object heureDebut;
    private Object heureFin;
    private double heureNuit;
    private double heureWeekEnd;
    private double heureNuitWeek;


    public int getIdPrestation() {
        return idPrestation;
    }

    public void setIdPrestation(int idPrestation) {
        this.idPrestation = idPrestation;
    }


    public Date getJour() {
        return jour;
    }

    public void setJour(Date jour) {
        this.jour = jour;
    }


    public Object getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(Object heureDebut) {
        this.heureDebut = heureDebut;
    }


    public Object getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(Object heureFin) {
        this.heureFin = heureFin;
    }


    public double getHeureNuit() {
        return heureNuit;
    }

    public void setHeureNuit(double heureNuit) {
        this.heureNuit = heureNuit;
    }


    public double getHeureWeekEnd() {
        return heureWeekEnd;
    }

    public void setHeureWeekEnd(double heureWeekEnd) {
        this.heureWeekEnd = heureWeekEnd;
    }


    public double getHeureNuitWeek() {
        return heureNuitWeek;
    }

    public void setHeureNuitWeek(double heureNuitWeek) {
        this.heureNuitWeek = heureNuitWeek;
    }


}
