package metier;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class EmploiH {
    private int idEmploi;
    private String libelleEmploi;

    public EmploiH() {

    }

    public int getIdEmploi() {
        return idEmploi;
    }

    public void setIdEmploi(int idEmploi) {
        this.idEmploi = idEmploi;
    }


    public String getLibelleEmploi() {
        return libelleEmploi;
    }

    public void setLibelleEmploi(String libelleEmploi) {
        this.libelleEmploi = libelleEmploi;
    }

    public final StringProperty emploiProperty() {
        return new SimpleStringProperty(this.libelleEmploi);
    }

    @Override
    public String toString() {
        return  libelleEmploi ;
    }
}
