package metier;


import javafx.beans.property.*;


import java.util.ArrayList;

public class PersonneH  {
    private IntegerProperty idPersonne;
    private StringProperty nom;
    private StringProperty prenom;
    private StringProperty dateDeNaissance;
    private StringProperty numeroSecuriteSociale;
    private StringProperty numeroCartePro;
    private StringProperty numeroTelephonePersonne;
    private StringProperty emailPersonne;
    private StringProperty sexe;
    private StringProperty adresse1;
    private StringProperty adresse2;
    private ArrayList<ContratH> contrats;
    private ObjectProperty<NationaliteH> nationalite;
    private VilleH ville;

    public PersonneH() {
        this.idPersonne = idPersonne;
this.idPersonne= new SimpleIntegerProperty();
        this.nom = new SimpleStringProperty();
        this.prenom = new SimpleStringProperty();
        this.dateDeNaissance = new SimpleStringProperty();
        this.numeroSecuriteSociale = new SimpleStringProperty();
        this.numeroCartePro = new SimpleStringProperty();
        this.numeroTelephonePersonne = new SimpleStringProperty();
        this.emailPersonne = new SimpleStringProperty();
        this.sexe = new SimpleStringProperty();
        this.adresse1 = new SimpleStringProperty();
        this.adresse2 = new SimpleStringProperty();
        this.contrats = new ArrayList<>();
        this.nationalite= new SimpleObjectProperty<>();


    }


    public  int getIdPersonne() {
        return this.idPersonneProperty().get();
    }
    public  void setIdPersonne(final int idGite) {
        this.idPersonneProperty().set(idGite);
    }
    public  IntegerProperty idPersonneProperty() {
        return this.idPersonne;
    }

    public String getNom() {
        return nomPerProperty().get();
    }
    public void setNom(String nom) { this.nomPerProperty().set(nom); }
    public  StringProperty nomPerProperty() {
        return this.nom;
    }


    public String getPrenom() {
        return prenomPerProperty().get();
    }
    public  void setPrenom(String prenom) { this.prenomPerProperty().set(prenom); }
    public  StringProperty prenomPerProperty() {
        return this.prenom;
    }

    public String getDateDeNaissance() { return dateNaissancePerProperty().get(); }
    public void setDateDeNaissance(String dateDeNaissance) { this.dateNaissancePerProperty().set(dateDeNaissance); }
    public  StringProperty dateNaissancePerProperty() { return this.dateDeNaissance; }

        public String getNumeroSecuriteSociale(){ return numeroSecuriteSocialeProperty().get(); }
    public void setNumeroSecuriteSociale(String numeroSecuriteSociale) { this.numeroSecuriteSocialeProperty().set(numeroSecuriteSociale); }
    public  StringProperty numeroSecuriteSocialeProperty() { return this.numeroSecuriteSociale; }


    public String getNumeroCartePro(){ return numeroCarteProProperty().get(); }
    public void setNumeroCartePro(String numeroCartePro){ this.numeroCarteProProperty().set(numeroCartePro); }
    public  StringProperty numeroCarteProProperty() { return this.numeroCartePro; }

    public String getNumeroTelephonePersonne(){ return telephoneProperty().get(); }
    public void setNumeroTelephonePersonne(String numeroTelephonePersonne){ this.telephoneProperty().set(numeroTelephonePersonne); }
    public  StringProperty telephoneProperty() { return this.numeroTelephonePersonne; }


    public String getEmailPersonne(){ return emailPersonneProperty().get(); }
    public void setEmailPersonne(String emailPersonne){ this.emailPersonneProperty().set(emailPersonne); }
    public  StringProperty emailPersonneProperty() { return this.emailPersonne; }

    public String getSexe(){ return sexeProperty().get(); }
    public void setSexe(String sexe){ this.sexeProperty().set(sexe); }
    public  StringProperty sexeProperty() { return this.sexe; }

    public String getAdresse1(){ return adresse1Property().get(); }
    public void setAdresse1(String adresse1){ this.adresse1Property().set(adresse1); }
    public  StringProperty adresse1Property() { return this.adresse1; }


    public String getAdresse2(){ return adresse2Property().get(); }

    public void setAdresse2(String adresse2){ this.adresse2Property().set(adresse2); }
    public  StringProperty adresse2Property() { return this.adresse2; }

    public ArrayList<ContratH> getContrats() {
        return contrats;
    }

    public void setContrats(ArrayList<ContratH> contrats) {
        this.contrats = contrats;
    }

    public NationaliteH getNationalite() {
        return nationalite.get();
    }

    public ObjectProperty<NationaliteH> nationaliteProperty() {
        return nationalite;
    }

    public void setNationalite(NationaliteH nationalite) {
        this.nationalite.set(nationalite);
    }

    public VilleH getVille() {
        return ville;
    }

    public void setVille(VilleH ville) {
        this.ville = ville;
    }




    @Override
    public String toString() {
        return "PersonneH{" +
                ", " + idPersonne.get() +
                ", " + nom.get()  +
                ", " + prenom.get()  +
                ", " + dateDeNaissance.get()  +
                ", " + numeroSecuriteSociale.get()  +
                ", " + numeroCartePro.get()  +
                ", " + numeroTelephonePersonne.get()  +
                ", "+ emailPersonne.get() +
                ", "+ sexe.get()  +
                ", " + adresse1.get()  +
                ", "+ adresse2.get()  +
                '}';
    }
}
