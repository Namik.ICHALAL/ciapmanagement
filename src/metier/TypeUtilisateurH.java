package metier;




public class TypeUtilisateurH {
    private int idTypeUtil;
    private String libelleTypeUtil;


    public int getIdTypeUtil() {
        return idTypeUtil;
    }

    public void setIdTypeUtil(int idTypeUtil) {
        this.idTypeUtil = idTypeUtil;
    }


    public String getLibelleTypeUtil() {
        return libelleTypeUtil;
    }

    public void setLibelleTypeUtil(String libelleTypeUtil) {
        this.libelleTypeUtil = libelleTypeUtil;
    }


}
