package metier;




public class EvenementH {
    private int idEvenement;
    private Object heure;
    private String libelleEvenement;
    private String remarque;

    public int getIdEvenement() {
        return idEvenement;
    }

    public void setIdEvenement(int idEvenement) {
        this.idEvenement = idEvenement;
    }


    public Object getHeure() {
        return heure;
    }

    public void setHeure(Object heure) {
        this.heure = heure;
    }


    public String getLibelleEvenement() {
        return libelleEvenement;
    }

    public void setLibelleEvenement(String libelleEvenement) {
        this.libelleEvenement = libelleEvenement;
    }

    public String getRemarque() {
        return remarque;
    }

    public void setRemarque(String remarque) {
        this.remarque = remarque;
    }


}
