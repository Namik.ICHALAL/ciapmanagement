package metier;


import javafx.beans.property.*;

public class ClientH {
    private IntegerProperty idClient;
    private StringProperty raisonSociale;
    private StringProperty numeroSiret;
    private StringProperty numeroTva;
    private StringProperty telephoneClient;
    private StringProperty emailClient;
    private StringProperty adresse1;
    private StringProperty adresse2;
    private StringProperty nom;
    private StringProperty prenom;
    private ObjectProperty<FormejuridiqueH> formejuridique;
    private VilleH ville;

    public ClientH() {
        this.idClient = new SimpleIntegerProperty();
        this.raisonSociale = new SimpleStringProperty();
        this.numeroSiret =new SimpleStringProperty();
        this.numeroTva = new SimpleStringProperty();
        this.telephoneClient = new SimpleStringProperty();
        this.emailClient = new SimpleStringProperty();
        this.adresse1 = new SimpleStringProperty();
        this.adresse2 = new SimpleStringProperty();
        this.nom = new SimpleStringProperty();
        this.prenom = new SimpleStringProperty();
        this.formejuridique= new SimpleObjectProperty<>();
        ville = new VilleH();
    }

    public int getIdClient(){ return idClientProperty().get(); }
    public void setIdClient(int idClient){ this.idClientProperty().set(idClient); }
    public  IntegerProperty idClientProperty() { return this.idClient; }

    public String getRaisonSociale(){ return raisonSocialeProperty().get(); }
    public void setRaisonSociale(String raisonSociale){ this.raisonSocialeProperty().set(raisonSociale); }
    public  StringProperty raisonSocialeProperty() { return this.raisonSociale; }

    public String getNumeroSiret(){ return numeroSiretProperty().get(); }
    public void setNumeroSiret(String numeroSiret){ this.numeroSiretProperty().set(numeroSiret); }
    public  StringProperty numeroSiretProperty() { return this.numeroSiret; }

    public String getNumeroTVA(){ return numeroTvaProperty().get(); }
    public void setNumeroTVA(String numeroTVA){ this.numeroTvaProperty().set(numeroTVA); }
    public  StringProperty numeroTvaProperty() { return this.numeroTva; }

    public String getNumeroTelephoneClient(){ return telephoneClientProperty().get(); }
    public void setNumeroTelephoneClient(String numeroTelephone){ this.telephoneClientProperty().set(numeroTelephone); }
    public  StringProperty telephoneClientProperty() { return this.telephoneClient; }

    public String getEmailClient(){ return emailClientProperty().get(); }
    public void setEmailClient(String email){ this.emailClientProperty().set(email); }
    public  StringProperty emailClientProperty() { return this.emailClient; }

    public String getAdresse1(){ return adresse1Property().get(); }
    public void setAdresse1(String adresse1){ this.adresse1Property().set(adresse1); }
    public  StringProperty adresse1Property() { return this.adresse1; }

    public String getAdresse2(){ return adresse2Property().get(); }
    public void setAdresse2(String adresse2){ this.adresse2Property().set(adresse2); }
    public  StringProperty adresse2Property() { return this.adresse2; }

    public String getNom() {
        return nomPerProperty().get();
    }
    public void setNom(String nom) { this.nomPerProperty().set(nom); }
    public  StringProperty nomPerProperty() {
        return this.nom;
    }


    public String getPrenom() {
        return prenomPerProperty().get();
    }
    public  void setPrenom(String prenom) { this.prenomPerProperty().set(prenom); }
    public StringProperty prenomPerProperty() {
        return this.prenom;
    }

    public FormejuridiqueH getFormejuridique() {
        return formejuridique.get();
    }

    public ObjectProperty<FormejuridiqueH> formejuridiqueProperty() {
        return formejuridique;
    }

    public void setFormejuridique(FormejuridiqueH formejuridique) {
        this.formejuridique.set(formejuridique);
    }

    public VilleH getVille() {
        return ville;
    }

    public void setVille(VilleH ville) {
        this.ville = ville;
    }

    @Override
    public String toString() {
        return "ClientH{" +
                idClient.get() +
                raisonSociale.get() +
                 numeroSiret.get() +
                 numeroTva.get() +
                 telephoneClient.get() +
                 emailClient.get() +
                 adresse1.get() +
                 adresse2.get() +
                nom.get() +
                 prenom.get() +
                formejuridique
               ;
    }
}
