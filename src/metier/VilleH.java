package metier;

public class VilleH {
    private int idVille;
    private String codeInsee;
    private String nomVille;
    private String codePostal;
private PaysH pays ;

    public VilleH() {

    }

    public int getIdVille() {
        return idVille;
    }

    public void setIdVille(int idVille) {
        this.idVille = idVille;
    }


    public String getCodeInsee() {
        return codeInsee;
    }

    public void setCodeInsee(String codeInsee) {
        this.codeInsee = codeInsee;
    }

    public String getNomVille() {
        return nomVille;
    }

    public void setNomVille(String nomVille) {
        this.nomVille = nomVille;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public PaysH getPays() {
        return pays;
    }

    public void setPays(PaysH pays) {
        this.pays = pays;
    }

    @Override
    public String toString() {
        return  nomVille ;
    }
}
