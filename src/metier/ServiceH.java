package metier;


import java.math.BigDecimal;

public class ServiceH {
    private int idPrestation;
    private String designation;
    private int nombreAgents;
    private int nobresHeure;
    private BigDecimal prixUnitaire;

    public int getIdPrestation() {
        return idPrestation;
    }

    public void setIdPrestation(int idPrestation) {
        this.idPrestation = idPrestation;
    }


    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }


    public int getNombreAgents() {
        return nombreAgents;
    }

    public void setNombreAgents(int nombreAgents) {
        this.nombreAgents = nombreAgents;
    }

    public int getNobresHeure() {
        return nobresHeure;
    }

    public void setNobresHeure(int nobresHeure) {
        this.nobresHeure = nobresHeure;
    }


    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }


}
