package metier;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FormejuridiqueH {
    private int idFormeJuridique;
    private String formeJuridique;
    private String acronyme;

    public FormejuridiqueH() {

    }

    public int getIdFormeJuridique() {
        return idFormeJuridique;
    }

    public void setIdFormeJuridique(int idFormeJuridique) {
        this.idFormeJuridique = idFormeJuridique;
    }


    public String getFormeJuridique() {
        return formeJuridique;
    }

    public void setFormeJuridique(String formeJuridique) {
        this.formeJuridique = formeJuridique;
    }


    public String getAcronyme() {
        return acronyme;
    }

    public void setAcronyme(String acronyme) {
        this.acronyme = acronyme;
    }

    public final StringProperty formeJuridiqueProperty() {
        return new SimpleStringProperty(this.formeJuridique);
    }

    @Override
    public String toString() {
        return "FormejuridiqueH{" +
                "formeJuridique='" + formeJuridique + '\'' +
                '}';
    }
}
