package metier;


import java.util.Date;

public class AutorisationtravailH {
    private int idAutorisation;
    private int numeroOrdreDocument;
    private String typeDocument;
    private Date debutAutorisation;
    private Date finAutorisation;


    public int getIdAutorisation() {
        return idAutorisation;
    }

    public void setIdAutorisation(int idAutorisation) {
        this.idAutorisation = idAutorisation;
    }

    public int getNumeroOrdreDocument() {
        return numeroOrdreDocument;
    }

    public void setNumeroOrdreDocument(int numeroOrdreDocument) {
        this.numeroOrdreDocument = numeroOrdreDocument;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }


    public Date getDebutAutorisation() {
        return debutAutorisation;
    }

    public void setDebutAutorisation(Date debutAutorisation) {
        this.debutAutorisation = debutAutorisation;
    }


    public Date getFinAutorisation() {
        return finAutorisation;
    }

    public void setFinAutorisation(Date finAutorisation) {
        this.finAutorisation = finAutorisation;
    }

}
