package metier;




public class PageH {
    private String idPage;
    private Object heureDebut;
    private Object heureFin;


    public String getIdPage() {
        return idPage;
    }

    public void setIdPage(String idPage) {
        this.idPage = idPage;
    }


    public Object getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(Object heureDebut) {
        this.heureDebut = heureDebut;
    }


    public Object getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(Object heureFin) {
        this.heureFin = heureFin;
    }


}
