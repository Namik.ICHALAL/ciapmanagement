package metier;


import java.util.Date;

public class MainCouranteH {
    private int idMainCourante;
    private String titreMainCourante;
    private Date dateDebut;
    private Date dateFin;


    public int getIdMainCourante() {
        return idMainCourante;
    }

    public void setIdMainCourante(int idMainCourante) {
        this.idMainCourante = idMainCourante;
    }


    public String getTitreMainCourante() {
        return titreMainCourante;
    }

    public void setTitreMainCourante(String titreMainCourante) {
        this.titreMainCourante = titreMainCourante;
    }


    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }


    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }


}
