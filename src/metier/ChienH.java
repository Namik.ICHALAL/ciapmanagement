package metier;

public class ChienH {
    private int idChien;
    private String nomChien;
    private String race;
    private String numeroProChien;

    public int getIdChien() {
        return idChien;
    }

    public void setIdChien(int idChien) {
        this.idChien = idChien;
    }

    public String getNomChien() {
        return nomChien;
    }

    public void setNomChien(String nomChien) {
        this.nomChien = nomChien;
    }


    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }


    public String getNumeroProChien() {
        return numeroProChien;
    }

    public void setNumeroProChien(String numeroProChien) {
        this.numeroProChien = numeroProChien;
    }


}
