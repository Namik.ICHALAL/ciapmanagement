package metier;




public class QualificationProfesionnelleH {
    private int idQualificationProfesionnelle;
    private String nomQualificationProfesionnelle;

    public QualificationProfesionnelleH() {

    }

    public int getIdQualificationProfesionnelle() {
        return idQualificationProfesionnelle;
    }

    public void setIdQualificationProfesionnelle(int idQualificationProfesionnelle) {
        this.idQualificationProfesionnelle = idQualificationProfesionnelle;
    }


    public String getNomQualificationProfesionnelle() {
        return nomQualificationProfesionnelle;
    }

    public void setNomQualificationProfesionnelle(String nomQualificationProfesionnelle) {
        this.nomQualificationProfesionnelle = nomQualificationProfesionnelle;
    }

    @Override
    public String toString() {
        return  nomQualificationProfesionnelle ;
    }
}
