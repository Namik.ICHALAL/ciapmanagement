package metier;


import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class NationaliteH {
    private int idNationalite;
    private String nomNationalite;

    public NationaliteH() {

    }

    public int getIdNationalite() {
        return idNationalite;
    }

    public void setIdNationalite(int idNationalite) {
        this.idNationalite = idNationalite;
    }


    public String getNomNationalite() {
        return nomNationalite;
    }

    public void setNomNationalite(String nomNationalite) {
        this.nomNationalite = nomNationalite;
    }
    public final StringProperty nationaliteProperty() {
        return new SimpleStringProperty(this.nomNationalite);
    }
    @Override
    public String toString() {
        return super.toString();
    }
}
