package dao;

import metier.PaysH;
import metier.VilleH;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class VilleDAO extends DAO<VilleH> {
    public VilleDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public VilleH getByID(int id) {
      VilleH ville = new VilleH();
        ResultSet rs;

        try
        {
            Statement stmt = null;



            stmt = connexion.createStatement();

            String strCmd = "SELECT v.id_ville,v.nom_ville,v.code_postal, v.code_insee, p.id_pays,p.nom_pays from ville  as v join pays p on v.id_pays = p.id_pays where v.id_ville="
                + id ;
            rs = stmt.executeQuery(strCmd);
            PaysH pays = new PaysH();
            while (rs.next()){
ville.setIdVille(rs.getInt(1));
ville.setNomVille(rs.getString(2));
ville.setCodePostal(rs.getString(3));
ville.setCodeInsee(rs.getString(4));
pays.setIdPays(rs.getInt(5));
pays.setNomPays(rs.getString(6));


            } } catch (Exception error) {
            System.out.println("impossible de récuperer la liste des employés ");
        }
        return ville;
    }




    @Override
    public ArrayList<VilleH> getAll() {

        ArrayList<VilleH> list = new ArrayList<VilleH>();
        ResultSet rs;

        try
        {
            Statement stmt = null;
            stmt = connexion.createStatement();
            String strCmd = "SELECT v.id_ville,v.nom_ville,v.code_postal, v.code_insee, p.id_pays,p.nom_pays from ville  as v join pays p on v.id_pays = p.id_pays"  ;
            rs = stmt.executeQuery(strCmd);
            while (rs.next()){
                VilleH ville = new VilleH();
                PaysH pays = new PaysH();
                ville.setIdVille(rs.getInt(1));
                ville.setNomVille(rs.getString(2));
                ville.setCodePostal(rs.getString(3));
                ville.setCodeInsee(rs.getString(4));
                pays.setIdPays(rs.getInt(5));
                pays.setNomPays(rs.getString(6));
   list.add(ville);
            } } catch (Exception error) {
            System.out.println("impossible de récuperer la liste des employés ");
        }
        return list;

    }

    @Override
    public boolean insert(VilleH objet) {
        return false;
    }

    @Override
    public boolean update(VilleH objet) {
        return false;
    }

    @Override
    public boolean delete(VilleH objet) {
        return false;
    }

    public ArrayList<VilleH> getByZipCode(String zipCode) {
        ArrayList<VilleH> list = new ArrayList<VilleH>();
        ResultSet rs;

        try
        {
            Statement stmt = null;
            stmt = connexion.createStatement();
            String strCmd = "SELECT v.id_ville,v.nom_ville,v.code_postal, v.code_insee, p.id_pays,p.nom_pays from ville  as v join pays p on v.id_pays = p.id_pays where code_postal like '" + zipCode +"'";
            rs = stmt.executeQuery(strCmd);
            System.out.println(strCmd);
            while (rs.next()){
                VilleH ville = new VilleH();
                PaysH pays = new PaysH();
                ville.setIdVille(rs.getInt(1));
                ville.setNomVille(rs.getString(2));
                ville.setCodePostal(rs.getString(3));
                ville.setCodeInsee(rs.getString(4));
                pays.setIdPays(rs.getInt(5));
                pays.setNomPays(rs.getString(6));
                list.add(ville);
            } } catch (Exception error) {
            System.out.println("impossible de récuperer la liste des  villes du code postal ");
        }
        System.out.println(list);
        return list;
            }
}
