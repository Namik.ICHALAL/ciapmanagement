package dao;

import metier.EmploiH;
import metier.TypeDeContratH;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class TypeContratDAO extends DAO<TypeDeContratH> {


    public TypeContratDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public TypeDeContratH getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<TypeDeContratH> getAll() {
        ResultSet rs;
        ArrayList<TypeDeContratH> liste = new ArrayList<TypeDeContratH>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT id_contrat, type_de_contrat from type_de_contrat ";

            rs = stmt.executeQuery(strCmd);



            while (rs.next()){
                TypeDeContratH typeContrat = new TypeDeContratH();
              typeContrat.setIdContrat(rs.getInt(1));
               typeContrat.setTypeDeContrat(rs.getString(2));
                liste.add(typeContrat);
            }
            rs.close();
        } catch (Exception error) {
            System.out.println("impossible de récuperer les types de Contact ");
        }
        return liste;
    }

    @Override
    public boolean insert(TypeDeContratH objet) {
        return false;
    }

    @Override
    public boolean update(TypeDeContratH objet) {
        return false;
    }

    @Override
    public boolean delete(TypeDeContratH objet) {
        return false;
    }
}
