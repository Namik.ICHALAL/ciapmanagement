package dao;


import java.sql.Connection;

public class DAOFactory {

    public static Connection connexion = DataBaseConnect.getInstance();

    public static PersonneDAO getPersonneDAO() {
        return new PersonneDAO(connexion);
    }

    public static EmploiDAO getEmploiDAO() {
        return new EmploiDAO(connexion);
    }
    public static ClientDAO getClientDAO() { return new ClientDAO(connexion); }
    public static VilleDAO getVilleDAO() { return new VilleDAO(connexion); }
    public static TypeContratDAO getTypeContratDAO() { return new TypeContratDAO(connexion); }
    public static QualificationProfessionnelleDAO getQualificationProfessionnelleDAO() { return new QualificationProfessionnelleDAO(connexion); }
}
