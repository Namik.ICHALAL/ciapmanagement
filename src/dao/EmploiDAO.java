package dao;

import metier.ContratH;
import metier.EmploiH;
import metier.NationaliteH;
import metier.PersonneH;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class EmploiDAO extends  DAO<EmploiH> {


    public EmploiDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public EmploiH getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<EmploiH> getAll() {
        ResultSet rs;
        ArrayList<EmploiH> liste = new ArrayList<EmploiH>();
        try
        {
            Statement stmt = null;



            stmt = connexion.createStatement();

            String strCmd = "SELECT id_emploi, libelle_emploi from emploi ";

            rs = stmt.executeQuery(strCmd);



            while (rs.next()){
                EmploiH emploi = new EmploiH();
                emploi.setIdEmploi(rs.getInt(1));
                emploi.setLibelleEmploi(rs.getString(2));
                liste.add(emploi);
            }
            rs.close();
        } catch (Exception error) {
            System.out.println("impossible de récuperer la liste des employés ");
        }
        return liste;
    }

    @Override
    public boolean insert(EmploiH objet) {
        return false;
    }

    @Override
    public boolean update(EmploiH objet) {
        return false;
    }

    @Override
    public boolean delete(EmploiH objet) {
        return false;
    }
}
