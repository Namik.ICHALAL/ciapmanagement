package dao;

import metier.*;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class PersonneDAO extends DAO<PersonneH>{
    public PersonneDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public PersonneH getByID(int id) {
        ResultSet rs;
        PersonneH personne= new PersonneH();
        try
        {
            Statement stmt = null;
            stmt = connexion.createStatement();

            String strCmd = "SELECT  p.nom,p.prenom,p.date_de_naissance,p.numero_securite_sociale, p.numero_carte_pro, p.numero_telephone_personne,\n" +
                    "                    p.email_personne,p.sexe, p.adresse_1, p.adresse2,p.id_personne,\n" +
                    "     n.id_nationalite,n.nom_nationalite, v.id_ville,v.code_insee,v.code_postal, v.nom_ville  from personne as p  \n" +
                    "                    join ville as v on v.id_ville = p.id_ville\n" +
                    "                    join Contrat as c on c.id_personne=p.id_personne\n" +
                    "                    JOIN emploi as e on c.id_emploi = e.id_emploi \n" +
                    "                    JOIN nationalite n on p.id_nationalite = n.id_nationalite\n" +
                    "                    where p.id_personne = "+ id ;
            rs = stmt.executeQuery(strCmd);
           

            while (rs.next()){
             
                NationaliteH nationalite = new NationaliteH();
                ContratH contrat = new ContratH();
                VilleH ville = new VilleH();
                personne.setIdPersonne(rs.getInt(11));
                personne.setNom(rs.getString(1));
                personne.setPrenom(rs.getString(2));
                personne.setDateDeNaissance(rs.getString(3));
                personne.setNumeroSecuriteSociale(rs.getString(4));
                personne.setNumeroCartePro(rs.getString(5));
                personne.setNumeroTelephonePersonne(rs.getString(6));
                personne.setEmailPersonne(rs.getString(7));
                personne.setSexe(rs.getString(8));
                personne.setAdresse1(rs.getString(9));
                personne.setAdresse2(rs.getString(10));
               
                nationalite.setIdNationalite(rs.getInt(12));
                nationalite.setNomNationalite(rs.getString(13));
                personne.setNationalite(nationalite);
                ville.setIdVille(rs.getInt(14));
                ville.setCodeInsee(rs.getString(15));
                ville.setCodePostal(rs.getString(16));
                ville.setNomVille(rs.getString(17));
                personne.setVille(ville);

                addContrats(personne);
            }
            rs.close();
        } catch (Exception error) {
            System.out.println("impossible de récuperer lles informations de l'enployé ");
        }
        return personne;
    }

    private void addChiensAgent(PersonneH personne) {
    }

    private void addAutorisationsTravail(PersonneH personne) {
    }

    private void addQualifications(PersonneH personne) {
    }

    @Override
    public ArrayList<PersonneH> getAll() {
        ResultSet rs;
        ArrayList<PersonneH> liste = new ArrayList<PersonneH>();
        try
        {
            Statement stmt = null;



            stmt = connexion.createStatement();

            String strCmd = "SELECT  p.nom,p.prenom,p.date_de_naissance,p.numero_securite_sociale, p.numero_carte_pro, p.numero_telephone_personne," +
                    "p.email_personne,p.sexe, p.adresse_1, p.adresse2,p.id_personne,n.id_nationalite,n.nom_nationalite from personne as p  join Contrat as c on c.id_personne=p.id_personne" +
                    " JOIN emploi as e on c.id_emploi = e.id_emploi " +
                    "JOIN nationalite n on p.id_nationalite = n.id_nationalite order by p.id_personne";
                    
            rs = stmt.executeQuery(strCmd);


            while (rs.next()){
                PersonneH personne= new PersonneH();
                NationaliteH nationalite = new NationaliteH();
                ContratH contrat = new ContratH();
                personne.setIdPersonne(rs.getInt(11));
personne.setNom(rs.getString(1));
            personne.setPrenom(rs.getString(2));
            personne.setDateDeNaissance(rs.getString(3));
            personne.setNumeroSecuriteSociale(rs.getString(4));
            personne.setNumeroCartePro(rs.getString(5));
            personne.setNumeroTelephonePersonne(rs.getString(6));
            personne.setEmailPersonne(rs.getString(7));
            personne.setSexe(rs.getString(8));
            personne.setAdresse1(rs.getString(9));
            personne.setAdresse2(rs.getString(10));
                liste.add(personne);
            addContrats(personne);
            nationalite.setIdNationalite(rs.getInt(12));
            nationalite.setNomNationalite(rs.getString(13));
            personne.setNationalite(nationalite);


        }
            rs.close();
    } catch (Exception error) {
            System.out.println("impossible de récuperer la liste des employés ");
        }
        return liste;}

    @Override
    public boolean insert(PersonneH objet) {
        return false;
    }

    @Override
    public boolean update(PersonneH objet) {
        return false;
    }

    @Override
    public boolean delete(PersonneH objet) {
        return false;
    }

    private void addContrats(PersonneH personne) {
        ResultSet rs;
        try
        {
            Statement stmt = null;
            stmt = connexion.createStatement();

            String strCmd =
"SELECT c.numero_contrat,c.date_debut,c.date_fin,e.id_emploi,e.libelle_emploi,tdc.id_contrat,tdc.type_de_contrat from personne as p \n" +
        "                    join Contrat as c on c.id_personne=p.id_personne\n" +
        "                    JOIN emploi as e on c.id_emploi = e.id_emploi\n" +
        "                     join type_de_contrat tdc on c.id_contrat = tdc.id_contrat\n" +
        "                    where c.id_personne ="+ personne.getIdPersonne()+" order by p.id_personne";
            rs = stmt.executeQuery(strCmd);
            EmploiH emploi = new EmploiH();
            ContratH contrat = new ContratH();
            TypeDeContratH typeContrat = new TypeDeContratH();
            while (rs.next()){
                contrat.setNumeroContrat(rs.getInt(1));
                contrat.setDateDebut(rs.getDate(2));
                contrat.setDateFin(rs.getDate(3));
                emploi.setIdEmploi(rs.getInt(4));
                emploi.setLibelleEmploi(rs.getString(5));
                typeContrat.setIdContrat(rs.getInt(6));
                typeContrat.setTypeDeContrat(rs.getString(7));
                contrat.setEmploi(emploi);
                contrat.setTypeContratH(typeContrat);
                personne.getContrats().add(contrat);
                System.out.println(personne.getContrats());
            }

        } catch (Exception error) {
            System.out.println("impossible de récuperer la liste des Contrats de la personne ");
        }

    }



}
