package dao;

import metier.ClientH;
import metier.FormejuridiqueH;
import metier.PaysH;
import metier.VilleH;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ClientDAO extends DAO<ClientH>  {
    public ClientDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public ClientH getByID(int id) {
        System.out.println(id);
        ClientH client = new ClientH();
        ResultSet rs;

        try
        {
            Statement stmt = null;



            stmt = connexion.createStatement();

            String strCmd = "SELECT   c.id_client, c.raison_sociale, c.numero_siret, c.numero_tva, c.telephone_client, " +
                    "c.email_client, c.adresse1, c.adresse2, c.nom, c.prenom, f.id_forme_juridique,f.forme_juridique,f.acronyme " +
                    ", v.id_ville,v.code_insee,v.code_postal,v.nom_ville,p.id_pays,p.nom_pays from client as c " +
                    "join formejuridique f on c.id_forme_juridique = f.id_forme_juridique join ville v on c.id_ville = v.id_ville join " +
                    "pays p on v.id_pays = p.id_pays where c.id_client = " + id ;
            rs = stmt.executeQuery(strCmd);
            FormejuridiqueH formejuridique = new FormejuridiqueH();
            VilleH ville = new VilleH();
            PaysH pays = new PaysH();
            while (rs.next()){


                client.setIdClient(rs.getInt(1));
                client.setRaisonSociale(rs.getString(2));
                client.setNumeroSiret(rs.getString(3));
                client.setNumeroTVA(rs.getString(4));
                client.setNumeroTelephoneClient(rs.getString(5));
                client.setEmailClient(rs.getString(6));
                client.setAdresse1(rs.getString(7));
                client.setAdresse2(rs.getString(8));
                client.setNom(rs.getString(9));
                client.setPrenom(rs.getString(10));
                formejuridique.setIdFormeJuridique(rs.getInt(11));
                formejuridique.setFormeJuridique(rs.getString(12));
                formejuridique.setAcronyme(rs.getString(13));
                client.setFormejuridique(formejuridique);
                ville.setIdVille(rs.getInt(14));
                ville.setCodeInsee(rs.getString(15));
                ville.setCodePostal(rs.getString(16));
                ville.setNomVille(rs.getString(17));
                client.setVille(ville);
                pays.setIdPays(rs.getInt(18));
                pays.setNomPays(rs.getString(19));
                client.getVille().setPays(pays);

            } } catch (Exception error) {
            System.out.println("impossible de récuperer la liste des employés ");
        }
        return client;
    }


    @Override
    public ArrayList<ClientH> getAll() {
        ResultSet rs;
        ArrayList<ClientH> liste = new ArrayList<ClientH>();
        try
        {
            Statement stmt = null;



            stmt = connexion.createStatement();

            String strCmd = "SELECT   c.id_client, c.raison_sociale, c.numero_siret, c.numero_tva, c.telephone_client, " +
                    "c.email_client, c.adresse1, c.adresse2, c.nom, c.prenom, f.id_forme_juridique,f.forme_juridique,f.acronyme from client as c " +
                    "join formejuridique f on c.id_forme_juridique = f.id_forme_juridique order  by c.id_client";
            rs = stmt.executeQuery(strCmd);
            while (rs.next()){
                ClientH client = new ClientH();
                FormejuridiqueH formejuridique = new FormejuridiqueH();
                client.setIdClient(rs.getInt(1));
                client.setRaisonSociale(rs.getString(2));
                client.setNumeroSiret(rs.getString(3));
                client.setNumeroTVA(rs.getString(4));
                client.setNumeroTelephoneClient(rs.getString(5));
                client.setEmailClient(rs.getString(6));
                client.setAdresse1(rs.getString(7));
                client.setAdresse2(rs.getString(8));
                client.setNom(rs.getString(9));
                client.setPrenom(rs.getString(10));
                formejuridique.setIdFormeJuridique(rs.getInt(11));
                formejuridique.setFormeJuridique(rs.getString(12));
                formejuridique.setAcronyme(rs.getString(13));
                client.setFormejuridique(formejuridique);
         liste.add(client);
            }
            System.out.println(liste);
            rs.close();
        } catch (Exception error) {
            System.out.println("impossible de récuperer la liste des employés ");
        }
        return liste;

    }

    @Override
    public boolean insert(ClientH objet) {
        return false;
    }

    @Override
    public boolean update(ClientH objet) {
        return false;
    }

    @Override
    public boolean delete(ClientH objet) {
        return false;
    }


}
