package dao;

import metier.EmploiH;
import metier.QualificationProfesionnelleH;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class QualificationProfessionnelleDAO extends  DAO<QualificationProfesionnelleH> {
    public QualificationProfessionnelleDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public QualificationProfesionnelleH getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<QualificationProfesionnelleH> getAll() {
        ResultSet rs;
        ArrayList<QualificationProfesionnelleH> liste = new ArrayList<QualificationProfesionnelleH>();
        try
        {
            Statement stmt = null;



            stmt = connexion.createStatement();

            String strCmd = "SELECT q.id_qualification_profesionnelle,q.nom_qualification_profesionnelle from qualification_profesionnelle as q";

            rs = stmt.executeQuery(strCmd);



            while (rs.next()){
                QualificationProfesionnelleH qualificationProfesionnelle = new QualificationProfesionnelleH();
                qualificationProfesionnelle.setIdQualificationProfesionnelle(rs.getInt(1));
                qualificationProfesionnelle.setNomQualificationProfesionnelle(rs.getString(2));
                liste.add(qualificationProfesionnelle);
            }
            rs.close();
        } catch (Exception error) {
            System.out.println("impossible de récuperer la liste des employés ");
        }
        System.out.println(liste);
        return liste;
    }

    @Override
    public boolean insert(QualificationProfesionnelleH objet) {
        return false;
    }

    @Override
    public boolean update(QualificationProfesionnelleH objet) {
        return false;
    }

    @Override
    public boolean delete(QualificationProfesionnelleH objet) {
        return false;
    }
}
