package services;


import dao.DAO;
import dao.DAOFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.*;

public class PersonnesBeans {
    private ObservableList<ClientH> clients;
    private ObservableList<PersonneH> salaries;
    private ObservableList<EmploiH> emploisList;
private ObservableList<QualificationProfesionnelleH> qualifications;
private ObservableList<TypeDeContratH> typeContrats;





    public PersonnesBeans() {
salaries = FXCollections.observableArrayList(DAOFactory.getPersonneDAO().getAll());
emploisList= FXCollections.observableArrayList(DAOFactory.getEmploiDAO().getAll());
clients = FXCollections.observableArrayList(DAOFactory.getClientDAO().getAll());
qualifications = FXCollections.observableArrayList(DAOFactory.getQualificationProfessionnelleDAO().getAll());
        typeContrats = FXCollections.observableArrayList(DAOFactory.getTypeContratDAO().getAll());
        System.out.println(clients);
    }

    public ObservableList<PersonneH> getSalaries() {
        return salaries;
    }

    public PersonneH getById(PersonneSearched personneSearched) {
        return  DAOFactory.getPersonneDAO().getByID(personneSearched.getIdPersonne());
    }

    public ObservableList<EmploiH> getEmploisList() {
        return emploisList;
    }

    public ObservableList<ClientH> getClients() {
        return clients;
    }

    public ObservableList<QualificationProfesionnelleH> getQualifications() {
        return qualifications;
    }

    public ObservableList<TypeDeContratH> getTypeContrats() {
        return typeContrats;
    }

    public ObservableList<VilleH> getCitiesZipCode(String text) {
        return  FXCollections.observableArrayList(DAOFactory.getVilleDAO().getByZipCode(text));
    }
}
