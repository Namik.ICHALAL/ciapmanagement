package services;

import metier.FormejuridiqueH;
import metier.VilleH;

public class ClientSearched {
    private int idClient;
    private String raisonSociale;
    private String numeroSiret;
    private String numeroTva;
    private String telephoneClient;
    private String emailClient;
    private String adresse1;
    private String adresse2;
    private String nom;
    private String prenom;
    private FormejuridiqueH formejuridique;
    private VilleH ville;

    public ClientSearched() {

    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public String getNumeroSiret() {
        return numeroSiret;
    }

    public void setNumeroSiret(String numeroSiret) {
        this.numeroSiret = numeroSiret;
    }

    public String getNumeroTva() {
        return numeroTva;
    }

    public void setNumeroTva(String numeroTva) {
        this.numeroTva = numeroTva;
    }

    public String getTelephoneClient() {
        return telephoneClient;
    }

    public void setTelephoneClient(String telephoneClient) {
        this.telephoneClient = telephoneClient;
    }

    public String getEmailClient() {
        return emailClient;
    }

    public void setEmailClient(String emailClient) {
        this.emailClient = emailClient;
    }

    public String getAdresse1() {
        return adresse1;
    }

    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }

    public String getAdresse2() {
        return adresse2;
    }

    public void setAdresse2(String adresse2) {
        this.adresse2 = adresse2;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public FormejuridiqueH getFormejuridique() {
        return formejuridique;
    }

    public void setFormejuridique(FormejuridiqueH formejuridique) {
        this.formejuridique = formejuridique;
    }

    public VilleH getVille() {
        return ville;
    }

    public void setVille(VilleH ville) {
        this.ville = ville;
    }
}
