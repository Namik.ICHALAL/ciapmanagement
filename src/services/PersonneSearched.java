package services;

import metier.*;

import java.util.ArrayList;

public class PersonneSearched {
    private int idPersonne;
    private String nom;
    private String prenom;
    private String dateDeNaissance;
    private String numeroSecuriteSociale;
    private String numeroCartePro;
    private String numeroTelephonePersonne;
    private String emailPersonne;
    private String sexe;
    private String adresse1;
    private String adresse2;
    private ArrayList<ContratH> contrats;
    private NationaliteH nationalite;
    private ArrayList<QualificationProfesionnelleH> qualifications;
    private VilleH ville;
    private ArrayList<AutorisationtravailH> autorisationtravails;
    private ArrayList<ChienH> chiens;
    public PersonneSearched() {

    }

    public int getIdPersonne() {
        return idPersonne;
    }

    public void setIdPersonne(int idPersonne) {
        this.idPersonne = idPersonne;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDateDeNaissance() {
        return dateDeNaissance;
    }

    public void setDateDeNaissance(String dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public String getNumeroSecuriteSociale() {
        return numeroSecuriteSociale;
    }

    public void setNumeroSecuriteSociale(String numeroSecuriteSociale) {
        this.numeroSecuriteSociale = numeroSecuriteSociale;
    }

    public String getNumeroCartePro() {
        return numeroCartePro;
    }

    public void setNumeroCartePro(String numeroCartePro) {
        this.numeroCartePro = numeroCartePro;
    }

    public String getNumeroTelephonePersonne() {
        return numeroTelephonePersonne;
    }

    public void setNumeroTelephonePersonne(String numeroTelephonePersonne) {
        this.numeroTelephonePersonne = numeroTelephonePersonne;
    }

    public String getEmailPersonne() {
        return emailPersonne;
    }

    public void setEmailPersonne(String emailPersonne) {
        this.emailPersonne = emailPersonne;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAdresse1() {
        return adresse1;
    }

    public void setAdresse1(String adresse1) {
        this.adresse1 = adresse1;
    }

    public String getAdresse2() {
        return adresse2;
    }

    public void setAdresse2(String adresse2) {
        this.adresse2 = adresse2;
    }

    public ArrayList<ContratH> getContrats() {
        return contrats;
    }

    public void setContrats(ArrayList<ContratH> contrats) {
        this.contrats = contrats;
    }

    public NationaliteH getNationalite() {
        return nationalite;
    }

    public void setNationalite(NationaliteH nationalite) {
        this.nationalite = nationalite;
    }

    public ArrayList<QualificationProfesionnelleH> getQualifications() {
        return qualifications;
    }

    public void setQualifications(ArrayList<QualificationProfesionnelleH> qualifications) {
        this.qualifications = qualifications;
    }

    public VilleH getVille() {
        return ville;
    }

    public void setVille(VilleH ville) {
        this.ville = ville;
    }

    public ArrayList<AutorisationtravailH> getAutorisationtravails() {
        return autorisationtravails;
    }

    public void setAutorisationtravails(ArrayList<AutorisationtravailH> autorisationtravails) {
        this.autorisationtravails = autorisationtravails;
    }

    public ArrayList<ChienH> getChiens() {
        return chiens;
    }

    public void setChiens(ArrayList<ChienH> chiens) {
        this.chiens = chiens;
    }



}
