package services;
import dao.DAOFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import metier.ClientH;


public class ClientBeans {

    private ObservableList<ClientH> clients;


    public ClientBeans() {
        this.clients = clients = FXCollections.observableArrayList(DAOFactory.getClientDAO().getAll());

    }

    public ObservableList<ClientH> getClients() {
        return clients;
    }



    public ClientH getClientById(int idClient) {
        System.out.println(idClient);
       return  DAOFactory.getClientDAO().getByID(idClient);
    }
}
