package app;

import controllers.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {
    private Stage primaryStage;
    private Stage loginStage;
    private Stage managmentChoicesStage;
    private LoginController loginController;
    private ManagementController managementController;
    private HomeController homeController;
    private PlanningController planningController;
    private RessourcesHController ressourcesHController;
    private ParametreController parametreController;
    private RegistreUniqueController registreUniqueController;
 private EditPersonneController editPersonneController;
private EditClientController editClientController;

// ...................start appliocation ....................

    public void start(Stage stage)  {
        primaryStage = stage;
        try {

            // Load the fxml file
            FXMLLoader loaderNew = new FXMLLoader();
            loaderNew.setLocation(Main.class.getResource("/vues/applicationStart.fxml"));
            BorderPane borderPane = new BorderPane();
            borderPane=loaderNew.load();
            // Creating the  Stage.
            primaryStage.setTitle("Ciap Managment ");

            primaryStage.getIcons().add(new Image("/icons/stageImage.png"));

            Scene scene = new Scene(borderPane);
            scene.getStylesheets().add(getClass().getResource("/Application.css").toExternalForm());
            primaryStage.setScene(scene);
            managementController = loaderNew.getController();
            managementController.setDialogStage(primaryStage);
            managementController.setMainApp(this);
            primaryStage.show();
            showLogin();
            if (loginController.isChecked()){

                showHome();
            }

        } catch (Exception error){
            System.out.println("lancement de la page impossible");
            return ;

        }
    }


    public void showLogin() {
        try {

            AnchorPane anchorPanelogin = new AnchorPane();
            FXMLLoader myFXMLloader = new FXMLLoader();
            myFXMLloader.setLocation(Main.class.getResource("/vues/connexionVue.fxml"));
            anchorPanelogin = myFXMLloader.load();
            Scene scene = new Scene(anchorPanelogin);
            Stage loginStage = new Stage();
            loginStage.initModality(Modality.WINDOW_MODAL);
            loginStage.initOwner(primaryStage);
            loginStage.setTitle("PROGICA CONNEXION");
            loginStage.setScene(scene);
            loginController = myFXMLloader.getController();
            loginController.setDialogStage(loginStage);
            loginController.setMain(this);

            loginStage.showAndWait();
            if (!loginController.isChecked()){loginStage.close();}

        } catch (Exception error)
        {
            System.out.println("connexion non oK!!!!!");

        }
    }


    public static void main(String[] args) {
        launch(args);
    }


    // ...................Set HomeScene for  PrimaryStage....................
    public void showHome() {
        try {

            // Load the fxml file
            FXMLLoader newLoader = new FXMLLoader();
            newLoader.setLocation(Main.class.getResource("/vues/home.fxml"));
            AnchorPane homeAnchorPane = new AnchorPane();
            homeAnchorPane=newLoader.load();

            // Creating the  Scene
            Scene homeScene = new Scene(homeAnchorPane);

            homeScene.getStylesheets().add(getClass().getResource("/ManagementChoicesStylesheets.css").toExternalForm());
            // set homeScene in the PrimaryStage
            primaryStage.setScene(homeScene);

            //Load the Controller
            homeController = newLoader.getController();
            homeController.setDialogStage(primaryStage);
            homeController.setMain(this);
          primaryStage.show();

        } catch (Exception error){
            System.out.println("lancement de la page impossible");
            return  ;

        }
    }
    public void showPlanning() {
        try {

            // Load the fxml file
            FXMLLoader newLoader = new FXMLLoader();
            newLoader.setLocation(Main.class.getResource("/vues/planning.fxml"));
            AnchorPane planningAnchorPane = new AnchorPane();
            planningAnchorPane=newLoader.load();
            // Creating the  Stage.
            Scene planningScene = new Scene(planningAnchorPane);
            planningScene.getStylesheets().add(getClass().getResource("/ManagementChoicesStylesheets.css").toExternalForm());
            primaryStage.setScene(planningScene);
           planningController = newLoader.getController();
            planningController.setDialogStage(primaryStage);
            planningController.setMain(this);
            primaryStage.show();

        } catch (Exception error){
            System.out.println("lancement de la page impossible");
            return ;

        }
    }

    // ...................Set RessourcesHumainesScene to the  PrimaryStage....................

    public void showRessourcesHumaines() {
        try {

            // Load the fxml file
            FXMLLoader newLoader = new FXMLLoader();
            newLoader.setLocation(Main.class.getResource("/vues/ressourcesHumaines.fxml"));
            AnchorPane rHAnchorPane = new AnchorPane();
            rHAnchorPane =newLoader.load();

            // Creating the  Scene
            Scene rHScene = new Scene(rHAnchorPane );

            rHScene.getStylesheets().add(getClass().getResource("/RH.css").toExternalForm());
            // set homeScene in the PrimaryStage
            primaryStage.setScene(rHScene);

            //Load the Controller
             ressourcesHController= newLoader.getController();
            ressourcesHController.setDialogStage(primaryStage);
            ressourcesHController.setMain(this);
            primaryStage.show();

        } catch (Exception error){
            System.out.println("lancement de la page impossible");
            return ;

        }
    }

    // ...................Set parametresScene to the  PrimaryStage....................


    public void showParametres() {
        try {

            // Load the fxml file
            FXMLLoader newLoader = new FXMLLoader();
            newLoader.setLocation(Main.class.getResource("/vues/parametres.fxml"));
            AnchorPane parametreAnchorPane = new AnchorPane();
            parametreAnchorPane =newLoader.load();

            // Creating the  Scene
            Scene parametreScene = new Scene( parametreAnchorPane );

            parametreScene.getStylesheets().add(getClass().getResource("").toExternalForm());
            // set homeScene in the PrimaryStage
            primaryStage.setScene(parametreScene);

            //Load the Controller
            parametreController= newLoader.getController();
            parametreController.setDialogStage(primaryStage);
            parametreController.setMain(this);
            primaryStage.show();


        } catch (Exception error){
            System.out.println("lancement de la page impossible");
            return ;

        }
    }
    // ...................Set registre unique Scene to the  PrimaryStage....................


    public void showRegistreUnique() {
        try {

            // Load the fxml file
            FXMLLoader newLoader = new FXMLLoader();
            newLoader.setLocation(Main.class.getResource("/vues/registreUnique.fxml"));
            AnchorPane registreAnchorPane = new AnchorPane();
            registreAnchorPane =newLoader.load();

            // Creating the  Scene
            Scene registreScene = new Scene(registreAnchorPane);

            registreScene.getStylesheets().add(getClass().getResource("").toExternalForm());
            // set homeScene in the PrimaryStage
            primaryStage.setScene(registreScene);

            //Load the Controller
            registreUniqueController= newLoader.getController();
            registreUniqueController.setDialogStage(primaryStage);
            registreUniqueController.setMain(this);
            primaryStage.show();


        } catch (Exception error){
            System.out.println("lancement de la page registre Unique impossible ");
            return ;

        }

    }
    //
    public void showEditPersonne(){
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/vues/editPersonne.fxml"));
            AnchorPane anchorPane = loader.load();
            Stage editStage = new Stage();
            editStage.setTitle("Edit");
            editStage.initModality(Modality.WINDOW_MODAL);
            editStage.initOwner(primaryStage);
            Scene scene = new Scene(anchorPane);
            editStage.setScene(scene);
            editPersonneController = loader.getController();
            editPersonneController.setMain(this);
            editPersonneController.setDialogStage(editStage);

            editStage.showAndWait();
        } catch (Exception error) {
            System.out.println("impossible de lancer edit Personne ");
        }

    }
    public void showEditClient(){
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("/vues/EditClient.fxml"));
            AnchorPane anchorPane = loader.load();
            Stage editStage = new Stage();
            editStage.setTitle("Edit");
            editStage.initModality(Modality.WINDOW_MODAL);
            editStage.initOwner(primaryStage);
            Scene scene = new Scene(anchorPane);
            editStage.setScene(scene);
            editClientController = loader.getController();
            editClientController.setMain(this);
            editClientController.setDialogStage(editStage);

            editStage.showAndWait();
        } catch (Exception error) {
            System.out.println("impossible de lancer edit Personne ");
        }

    }
}
